''' fix2fact.py a fixture to factory code generator'''
import json
import os
import sys

#{
#     u'pk': 1076,
#     u'model': u'shows.barcode',
#     u'fields': {
#         u'item': 12750,
#         u'code': 14381,
#         u'qtyprntd': 27,
#         u'qtysold': 0,
#         u'serltnum': u''
#     }
# }


def fn_from_file(fpath):
    '''given a file name, s, return the basename w/o extension, normalized
    for use as a python function name'''
    return os.path.splitext(os.path.basename(fpath)
                            )[0].replace('-', '_'
                            ).replace('.', '_')


def main(fixture):
    '''make factories out of your fixtures'''

    print "'''auto created model factories'''"
    print "import factory"
    print

    # fixture = 'initial_data.json'
    mdls = {}
    j_data = json.loads(open(fixture).read())
    for item in j_data:
        if item['model'] not in mdls:
            app, mdl = item['model'].split('.')
            mdls[item['model']] = {
                            'app': app,
                            'mdl': mdl,
                            'factory': '%sFactory' % mdl.title(),
                            'fields': item['fields']
                            }
            print "from %s.models import %s" % (app, mdl.title())
    print
    print

    for m_name in mdls:
        print "class %s(factory.Factory):" % mdls[m_name]['factory']
        print "   FACTORY_FOR = %s" % mdls[m_name]['mdl'].title()
        print
        for key in mdls[m_name]['fields']:
            print "   #%s = %s " % (key, mdls[m_name]['fields'][key])
        print
        print

    print "def load_%s():" % fn_from_file(fixture)
    print "    '''load old fixture '%s' via factories'''" % fixture
    print
    for item in j_data:
        print "    %s_%s = %s(pk=%s,**%s)" % (mdls[item['model']]['mdl'],
                                          item['pk'],
                                          mdls[item['model']]['factory'],
                                  item['pk'],
                                  item['fields']
                                  )


if __name__ == '__main__':
    fname = sys.argv[1]
    main(fname)
