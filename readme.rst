===================
fixture_to_factory
===================
**simplistic** code generator to convert django fixtures to factory_boy based factories.

When moving a project in whole or in part from fixtures to factories for testing data it can be helpful to quickly reconstruct your fixtures as factories.

It does not do the following:

 * smart sequencing of required field

 * care about or handle foreign keys

 * care about or handle m2m relations (see above)

 * make ponies fly


It just recreates your fixtures in factory style.  You will need to trim down the amount of data, give sensible defaults for required fields, implement sequences as needed for smart auto-generation.  What it does do, is give you something to start hacking on right now and then refine as you transition from fixtures to factories.


usage
======

.. sourcecode:: sh

  $ fix2fact.py myfixture.json > myfixture.py


motivation
===========
I wanted my tests faster less brittle/fragile.  Was watching a pycon 2012 video on `testing <https://pycon-2012-notes.readthedocs.org/en/latest/testing_and_django.html#or-use-factory-boy>`_ and one of the topics was Factory classes for django models.  The presenter explicitly mentioned `factory_boy <https://factoryboy.readthedocs.org/en/latest/>`_ and I was intrigued because it was suppose to solve some of the pain points I had encountered with fixtures.

However, I have all these fixtures and I'm lazy.  The Factories looked pretty boiler plate and being a programmer ... well you know what comes next.

Example fixtures
=================
myapp.person.json example from https://docs.djangoproject.com/en/dev/howto/initial-data/

running:

.. sourcecode:: sh

  $ fix2fact.py myapp.person.json

outputs the following:

.. sourcecode:: python

    '''auto created model factories'''
    import factory

    from myapp.models import Person


    class PersonFactory(factory.Factory):
       FACTORY_FOR = Person

       #first_name = John
       #last_name = Lennon


    def load_myapp_person():
        '''load old fixture 'fixtures/myapp.person.json' via factories'''

        person_1 = PersonFactory(pk=1,**{u'first_name': u'John', u'last_name': u'Lennon'})
        person_2 = PersonFactory(pk=2,**{u'first_name': u'Paul', u'last_name': u'McCartney'})


Problems with Fixtures Solved by Factory
========================================
http://stackoverflow.com/questions/4002322/migrating-django-fixtures

Contributing
=============
Please file any issues here on the bitbucket issue tracker, and if capable/willing pull requests are gladly entertained.

